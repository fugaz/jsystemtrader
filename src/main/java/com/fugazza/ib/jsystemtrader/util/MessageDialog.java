package com.fugazza.ib.jsystemtrader.util;

import com.fugazza.ib.jsystemtrader.client.*;

import javax.swing.*;

/**
 * Utility class
 */
public class MessageDialog {

    public static void showMessage(String msg) {
        JOptionPane.showMessageDialog(null, msg, JSystemTrader.APP_NAME, JOptionPane.INFORMATION_MESSAGE);
    }

    public static void showError(String msg) {
        JOptionPane.showMessageDialog(null, msg, JSystemTrader.APP_NAME, JOptionPane.ERROR_MESSAGE);
    }
}
